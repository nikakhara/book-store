package com.btust.bookstore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fantastic.*

class FantasticActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fantastic)
        init()
    }
    private fun init() {
        buyButton.setOnClickListener() {
            val intent = Intent(this, BuyActivity::class.java)
            startActivity(intent)
        }
    }
}
