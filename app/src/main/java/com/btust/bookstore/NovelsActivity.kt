package com.btust.bookstore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.portrait0
import kotlinx.android.synthetic.main.activity_main.portrait1
import kotlinx.android.synthetic.main.activity_main.portrait2
import kotlinx.android.synthetic.main.activity_main.portrait3
import kotlinx.android.synthetic.main.activity_main.portrait4
import kotlinx.android.synthetic.main.activity_novels.*

class NovelsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_novels)
        init()
    }

    private fun init(){
        portrait0.setOnClickListener(){
            val intent = Intent (this, UlissesActivity::class.java)
            startActivity(intent)
        }

        portrait1.setOnClickListener(){
            val intent1 = Intent (this, NewWorldActivity::class.java)
            startActivity(intent1)
        }

        portrait2.setOnClickListener(){
            val intent2 = Intent (this, GreatGatsbyActivity::class.java)
            startActivity(intent2)
        }

        portrait3.setOnClickListener(){
            val intent3 = Intent (this, LolitaActivity::class.java)
            startActivity(intent3)
        }

        portrait4.setOnClickListener(){
            val intent4 = Intent (this, SoundActivity::class.java)
            startActivity(intent4)
        }

        portrait5.setOnClickListener(){
            val intent5 = Intent (this, WinesActivity::class.java)
            startActivity(intent5)
        }

        portrait6.setOnClickListener(){
            val intent6 = Intent (this, SoldierActivity::class.java)
            startActivity(intent6)
        }

        portrait7.setOnClickListener(){
            val intent7 = Intent (this, TenderActivity::class.java)
            startActivity(intent7)
        }

    }

}
