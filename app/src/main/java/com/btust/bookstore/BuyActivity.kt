package com.btust.bookstore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_buy.*

class BuyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy)
        check()
    }

    private fun check(){
        payButton.setOnClickListener{
            if (idEditText.text.isEmpty() or cvvEditText.text.isEmpty() or yearEditText.text.isEmpty() or monthEditText.text.isEmpty()) {
                Toast.makeText(this,"Something went wrong",Toast.LENGTH_SHORT).show()
            }
            else {
                Toast.makeText(this,"Transaction complete",Toast.LENGTH_SHORT).show()
            }
        }
    }
}
