package com.btust.bookstore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        Glide.with(this)
            .load("https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F66654343%2F140350290676%2F1%2Foriginal.20190726-160931?w=1000&auto=format%2Ccompress&q=75&sharp=10&rect=0%2C0%2C1440%2C720&s=62647115de4963aaf36fcb1d4e04f6b4")
            .into(backgroundImageView)

    }

    private fun init() {
        profileButton.setOnClickListener(){
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        portrait0.setOnClickListener() {
            val intent = Intent(this, UlissesActivity::class.java)
            startActivity(intent)
        }

        portraitMoreButton.setOnClickListener() {
            val intent1 = Intent(this, NovelsActivity::class.java)
            startActivity(intent1)
        }

        portrait1.setOnClickListener() {
            val intent2 = Intent(this, NewWorldActivity::class.java)
            startActivity(intent2)
        }

        portrait2.setOnClickListener() {
            val intent3 = Intent(this, GreatGatsbyActivity::class.java)
            startActivity(intent3)
        }

        portrait3.setOnClickListener() {
            val intent4 = Intent(this, LolitaActivity::class.java)
            startActivity(intent4)
        }

        portrait4.setOnClickListener() {
            val intent5 = Intent(this, SoundActivity::class.java)
            startActivity(intent5)
        }

        comicBooksMoreButton.setOnClickListener() {
            val intent6 = Intent(this, ComicsActivity::class.java)
            startActivity(intent6)
        }

        comicBooks0.setOnClickListener() {
            val intent6 = Intent(this, BatmanActivity::class.java)
            startActivity(intent6)
        }

        comicBooks1.setOnClickListener() {
            val intent1 = Intent(this, DeadpoolActivity::class.java)
            startActivity(intent1)
        }

        comicBooks2.setOnClickListener(){
            val intent2 = Intent (this, IronActivity::class.java)
            startActivity(intent2)
        }

        comicBooks3.setOnClickListener(){
            val intent3 = Intent (this, SpiderAcitivty::class.java)
            startActivity(intent3)
        }

        comicBooks4.setOnClickListener(){
            val intent4 = Intent (this, WonderActivity::class.java)
            startActivity(intent4)
        }

        fantasyButton0.setOnClickListener() {
            val intent = Intent(this, DuneActivity::class.java)
            startActivity(intent)
        }

        fantasyButton1.setOnClickListener() {
            val intent = Intent(this, WitcherActivity::class.java)
            startActivity(intent)
        }

        fantasyButton2.setOnClickListener() {
            val intent = Intent(this, MetroActivity::class.java)
            startActivity(intent)
        }

        fantasyButton3.setOnClickListener() {
            val intent = Intent(this, Metro34Activity::class.java)
            startActivity(intent)
        }

        fantasyButton4.setOnClickListener() {
            val intent = Intent(this, LordActivity::class.java)
            startActivity(intent)
        }

        fantasyMoreButton.setOnClickListener() {
            val intent = Intent(this, FantasyActivity::class.java)
            startActivity(intent)
        }

        bestSellers0.setOnClickListener(){
            val intent = Intent (this, HarryActivity::class.java)
            startActivity(intent)
        }

        bestSellers1.setOnClickListener(){
            val intent = Intent (this, PantherActivity::class.java)
            startActivity(intent)
        }

        bestSellers2.setOnClickListener(){
            val intent = Intent (this, Harry2Activity::class.java)
            startActivity(intent)
        }

        bestSellers3.setOnClickListener(){
            val intent = Intent (this, PermanentActivity::class.java)
            startActivity(intent)
        }

        bestSellers4.setOnClickListener(){
            val intent = Intent (this, GutsActivity::class.java)
            startActivity(intent)
        }

        bestSellersMoreButton.setOnClickListener(){
            val intent = Intent (this, BestsellersActivity::class.java)
            startActivity(intent)
        }

    }

}
