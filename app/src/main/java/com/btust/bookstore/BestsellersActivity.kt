package com.btust.bookstore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_bestsellers.*

class BestsellersActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bestsellers)
        init()
    }

    private fun init(){
        bestSellers0.setOnClickListener(){
            val intent = Intent (this, HarryActivity::class.java)
            startActivity(intent)
        }

        bestSellers1.setOnClickListener(){
            val intent = Intent (this, PantherActivity::class.java)
            startActivity(intent)
        }

        bestSellers2.setOnClickListener(){
            val intent = Intent (this, Harry2Activity::class.java)
            startActivity(intent)
        }

        bestSellers3.setOnClickListener(){
            val intent = Intent (this, PermanentActivity::class.java)
            startActivity(intent)
        }

        bestSellers4.setOnClickListener(){
            val intent = Intent (this, GutsActivity::class.java)
            startActivity(intent)
        }

    }

}
