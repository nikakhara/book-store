package com.btust.bookstore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fantasy.*

class FantasyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fantasy)
        init()
    }

    private fun init() {
        fantasy0.setOnClickListener() {
            val intent = Intent(this, DuneActivity::class.java)
            startActivity(intent)
        }

        fantasy1.setOnClickListener() {
            val intent = Intent(this, WitcherActivity::class.java)
            startActivity(intent)
        }

        fantasy2.setOnClickListener() {
            val intent = Intent(this, MetroActivity::class.java)
            startActivity(intent)
        }

        fantasy3.setOnClickListener() {
            val intent = Intent(this, Metro34Activity::class.java)
            startActivity(intent)
        }

        fantasy4.setOnClickListener() {
            val intent = Intent(this, LordActivity::class.java)
            startActivity(intent)
        }

    }

}
