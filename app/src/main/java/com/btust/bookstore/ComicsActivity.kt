package com.btust.bookstore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_comics.*

class ComicsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comics)
        init()
    }

    private fun init() {
        comics0.setOnClickListener() {
            val intent = Intent(this, BatmanActivity::class.java)
            startActivity(intent)
        }

        comics1.setOnClickListener() {
            val intent1 = Intent(this, DeadpoolActivity::class.java)
            startActivity(intent1)
        }

        comics2.setOnClickListener(){
            val intent2 = Intent (this, IronActivity::class.java)
            startActivity(intent2)
        }

        comics3.setOnClickListener(){
            val intent3 = Intent (this, SpiderAcitivty::class.java)
            startActivity(intent3)
        }

        comics4.setOnClickListener(){
            val intent4 = Intent (this, WonderActivity::class.java)
            startActivity(intent4)
        }

        comics5.setOnClickListener(){
            val intent5 = Intent (this, ThorActivity::class.java)
            startActivity(intent5)
        }

        comics6.setOnClickListener(){
            val intent6 = Intent (this, CaptainActivity::class.java)
            startActivity(intent6)
        }

        comics7.setOnClickListener(){
            val intent7 = Intent (this, FantasticActivity::class.java)
            startActivity(intent7)
        }

    }

}
