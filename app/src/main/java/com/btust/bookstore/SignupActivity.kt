package com.btust.bookstore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        auth = FirebaseAuth.getInstance()
        init()
    }

    private fun init() {
        signupButton.setOnClickListener() {
            if (emailEditText.text.toString().isEmpty() or passwordEditText.text.toString().isEmpty() or password2EditText.text.toString().isEmpty()) {
                signupButton.isClickable = false
                signupButton.isClickable = true
            } else {
                auth.createUserWithEmailAndPassword(
                    emailEditText.text.toString(),
                    passwordEditText.text.toString()
                )
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Success", "createUserWithEmail:success")
                            val user = auth.currentUser
                            Toast.makeText(
                                baseContext, "Sign up success.",
                                Toast.LENGTH_SHORT
                            ).show()
                            val intent = Intent(this, LoginActivity::class.java)
                            startActivity(intent)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Fail", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Sign up failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }


        }
    }
}
